# FINE_util.py

# SPDX-License-Identifier: Apache-2.0

import teamtopology as tt
import fineflowevaluation as fine
import flowratio as fr
import json
import sys

if __name__ == '__main__':
    
    filename = sys.argv[1]
    with open(filename, 'r') as f:
        data = json.load(f)
        ttopology = fine.evaluate(data, flow=True, imp=True, need=True, energy=True, resilience=True)
   
    print("{:<12} {:<8} {:<8} {:<8} {:<8} {:<8}".format('Team','Flow','Imps','Need','Energy','Resilience'))
    for t in ttopology:
        print("{:<12} {:<8} {:<8} {:<8} {:<8} {:<8}".format(t,*ttopology[t]))
