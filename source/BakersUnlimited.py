# BakersUnlimited.py

# SPDX-License-Identifier: Apache-2.0

import teamtopology as tt
import fineflowevaluation as fine
import flowratio as fr
import json

#Team flow for Bakers Unlimited Example
BU_TeamFlow = {'StoreRO':  ['CRM', 'QE', 'DataEC'],
                            'OnlineRO': ['CRM', 'UX', 'QE', 'DataEC'],
                            'CRM':      ['DataEC', 'CloudES'],
                            'UX':       [],
                            'QE':       [],
                            'DataEC':   ['QE', 'CloudES'],
                            'CloudES':  ['QE']
        }

#Team flow for Bakers Unlimited Example with interactions    
BU_TeamFlowWithInteractions = {'StoreRO':  [('CRM', 'C'), ('QE', 'F'), ('DataEC', 'X')],
                                            'OnlineRO': [('CRM', 'C'), ('UX', 'F'), ('QE', 'F'), ('DataEC', 'X')],
                                            'CRM':      [('DataEC', 'X'), ('CloudES', 'X')],
                                            'UX':       [],
                                            'QE':       [],
                                            'DataEC':   [('QE', 'F'), ('CloudES', 'X')],
                                            'CloudES':  [('QE', 'F')]
        }

#Team flow for Bakers Unlimited Example focused on Value Stream Map for OnlineRO 
BU_TeamFlowOnlineRO = {'OnlineRO': [('UX', 'F'), ('QE', 'F'), ('DataEC', 'X')],
                            'UX':       [],
                            'QE':       [],
                            'DataEC':   [('QE', 'F'), ('CloudES', 'X')],
                            'CloudES':  [('QE', 'F')]
}

#Team flow for Bakers Unlimited Example focused on Value Stream Map for OnlineRO with QE team using x-as-a-service
BU_TeamFlowOnlineROWithQEX = {'OnlineRO':    [('UX', 'F'), ('QE', 'X'), ('DataEC', 'X')],
                                    'UX':          [],
                                    'QE':          [],
                                    'DataEC':      [('QE', 'X'), ('CloudES', 'X')],
                                    'CloudES':     [('QE', 'X')]
}

#Team flow for Bakers Unlimited Example with interactions with QE team using x-as-a-service
BU_TeamFlowWithInteractionsWithQEX = {'StoreRO':  [('CRM', 'C'), ('QE', 'X'), ('DataEC', 'X')],
                                            'OnlineRO': [('CRM', 'C'), ('UX', 'F'), ('QE', 'X'), ('DataEC', 'X')],
                                            'CRM':      [('DataEC', 'X'), ('CloudES', 'X')],
                                            'UX':       [],
                                            'QE':       [],
                                            'DataEC':   [('QE', 'X'), ('CloudES', 'X')],
                                            'CloudES':  [('QE', 'X')]
}

#Team flow for Bakers Unlimited Example with interactions
BU_TeamFlowWithEmbeddedQE =   {'StoreRO':     [('CRM', 'C'), ('QE', 'F'), ('DataEC', 'X')],
                                    'OnlineRO':    [('CRM', 'C'), ('UX', 'F'), ('OnlineRO_QE', 'F'), ('DataEC', 'X')],
                                    'OnlineRO_QE': [('QE', 'C')],
                                    'CRM':         [('DataEC', 'X'), ('CloudES', 'X')],
                                    'UX':          [],
                                    'QE':          [],
                                    'DataEC':      [('QE', 'F'), ('CloudES', 'X')],
                                    'CloudES':     [('QE', 'F')]
}

#Team flow for Bakers Unlimited Example with interactions
BU_TeamFlowWithEmbeddedQE_AllF =   {'StoreRO': [('CRM', 'C'), ('QE', 'F'), ('DataEC', 'X')],
                                    'OnlineRO':    [('CRM', 'C'), ('UX', 'F'), ('OnlineRO_QE', 'F'), ('DataEC', 'X')],
                                    'OnlineRO_QE': [('QE', 'F')],
                                    'CRM':         [('DataEC', 'X'), ('CloudES', 'X')],
                                    'UX':          [],
                                    'QE':          [],
                                    'DataEC':      [('QE', 'F'), ('CloudES', 'X')],
                                    'CloudES':     [('QE', 'F')]
}

if __name__ == '__main__':
    # ttopology = tt.findTopology(BU_TeamFlow)
    # ttopology = fine.evaluate(BU_TeamFlowOnlineRO, flow=True, imp=True, need=True, energy=True, resilience=True)
    # ttopology = fine.evaluate(BU_TeamFlowOnlineROWithQEX, flow=True, imp=True, need=True, energy=True, resilience=True)
    # ttopology = fine.evaluate(BU_TeamFlowWithInteractionsWithQEX, flow=True, imp=True, need=True, energy=True, resilience=True)
    # ttopology = fine.evaluate(BU_TeamFlowWithEmbeddedQE, flow=True, imp=True, need=True, energy=True, resilience=True)
    # ttopology = fine.evaluate(BU_TeamFlowWithEmbeddedQE_AllF, flow=True, imp=True, need=True, energy=True, resilience=True)

    ttopology = fine.evaluate(BU_TeamFlowWithInteractions, flow=True, imp=True, need=True, energy=True, resilience=True)

    # flowEntropy = fr.computeEntropy(bad=1, good=10, batchSize=1, cycles=10, imps=0.0599, energy=0.625, fixedFlow=True, energyMax=1)
    # flowEntropy = fr.computeEntropy(bad=1, good=10, batchSize=1, cycles=10, imps=0.055, energy=0.65, fixedFlow=True, energyMax=1)

    print(json.dumps(ttopology,sort_keys=True, indent=4))